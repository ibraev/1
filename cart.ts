import {createSlice} from '@reduxjs/toolkit';
import {checkOutAction} from './cart.action';
import {CartProductType} from '../type';

type CartState = {
  cartList: CartProductType[];
  loading: boolean;
  error: string | undefined;
  totalCost: number;
  sum: number;
  countItems: number;
  checkOut: string | undefined;
  totalPay: number;
  shipping: number;
};

const initialState: CartState = {
  cartList: [],
  loading: false,
  error: undefined,
  totalCost: 0,
  countItems: 0,
  sum: 0,
  checkOut: '',
  totalPay: 0,
  shipping: 0,
};
const CartSlice = createSlice({
  name: 'cart',
  initialState: initialState,
  reducers: {
    // cart methods
    addProductToCartAction(state, action) {
      const product = state.cartList.find(
        item => item.id === action.payload.id,
      );
      if (product) {
        if (product.quantity !== undefined) {
          product.quantity++;
        }
      } else {
        state.cartList.push({...action.payload, quantity: 1});
      }
    },
    increaseProductAction(state, action) {
      const product = state.cartList.find(item => item.id === action.payload);
      if (product && product.quantity !== undefined) {
        product.quantity++;
      } else {
        state.cartList.push({...action.payload, quantity: 1});
      }
    },
    decreaseProductAction(state, action) {
      const product = state.cartList.find(item => item.id === action.payload);
      if (product && product.quantity !== undefined && product.quantity > 1) {
        product.quantity--;
      } else {
        state.cartList = state.cartList.filter(
          item => item.id !== action.payload,
        );
      }
    },
    setTotalCostAction(state, action) {
      state.totalCost = action.payload;
    },
    deleteProductCartAction: (state, action) => {
      state.cartList = state.cartList.filter(
        item => item.id !== action.payload,
      );
    },
    clearCartAction(state) {
      state.cartList = [];
    },
    setTotalPayAction(state) {
      state.totalPay = state.totalCost + state.shipping;
    },
  },
  extraReducers: builder => {
    builder
      .addCase(checkOutAction.pending, state => {
        state.loading = true;
      })
      .addCase(checkOutAction.fulfilled, (state, action) => {
        state.loading = false;
        state.checkOut = action.payload;
      })
      .addCase(checkOutAction.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const {
  addProductToCartAction,
  setTotalCostAction,
  deleteProductCartAction,
  decreaseProductAction,
  clearCartAction,
  increaseProductAction,
  setTotalPayAction,
} = CartSlice.actions;
export default CartSlice.reducer;
